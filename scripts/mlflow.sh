#!/bin/bash

HOST=0.0.0.0
PORT=5000
STORE_URI=models


# Validations
if ! [ -x "$(command -v mlflow)" ]; then
    echo "Error: mlflow is not installed."
    exit 1
fi

# Print parameters
echo "MLflow will start with the following parameters"
echo "Host: $HOST"
echo "Port: $PORT"
echo "Model storage: $STORE_URI"


# Start service
mlflow ui -h $HOST -p $BONUS_PORT --backend-store-uri $STORE_URI&
