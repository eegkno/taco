#!/bin/bash

start() {
    echo "Start MLflow UI"
    bash scripts/mlflow.sh
}

stop() {
    echo "Shutdown MLflow UI"
    kill -9 $(ps -ef | grep mlflow | awk '{print $2}')
}


case "$1" in
start)
    start
    ;;
stop)
    stop
    ;;
status)
    echo "Mlflow status"
    ps -eaf | grep mlflow
    ;;
*)
   echo "Usage: $0 {start|stop|status}"
esac

exit 0

