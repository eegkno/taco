# Makefile

# This generates a symbolic link (shortcut) where all the data is shared.
datalink:
	mkdir data

# Create a symbolic link to model directory on labshare:
modellink:
	mkdir models

# This creates a conda environment named as taco
# There are two options, installing from a yaml or requirements files

# Create env and install libraries from yaml file using conda
env_yaml:
	conda env create -f requirements.yaml --prefix $(HOME)/.conda/envs/taco

# Install python libraries for development
pydev:
	pip install -r requirements-dev.txt
	pip install ipykernel
	ipython kernel install --user --name taco

# Any other Linux libraries to be installed
install:
	echo "To be defined"
