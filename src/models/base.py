""" Module with base model template """

from pathlib import Path
from typing import Dict, Union


class BaseModel:
    """ Bade model template """

    def __init__(self, params: Dict):
        self.model = None
        self.params = params

    def fit(self, train, valid=None, **kwargs):
        """
        Train model

        Parameters:
        -----------
        train:
            Training data
        valid:
            Validation data

        Returns:
        --------
        """

    def predict(self, test):
        """
        Predict categories

        Parameters:
        -----------
        test:
            Test data

        Returns
        -------
        numpy.array
        """

    def predict_proba(self, test):
        """
        Predict porbabilities

        Parameters
        ----------
        test:
            Test data

        Returns
        -------
        numpy.array
        """

    def save(self, path_to_model: Union[str, Path]):
        """
        Save model

        Parameters
        ----------
        path_to_model: Union[str, Path]

        Returns
        -------
        None
        """

    def load(self, path_to_model: Union[str, Path]):
        """
        Save model

        Parameters
        ----------
        path_to_model: Union[str, Path]

        Returns
        -------
        """
