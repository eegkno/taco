""" Module to train a benchmark model """
import logging
from pathlib import Path
from typing import Optional, Union

import fire
from src.data.pipeline import create_subsets
from src.models.benchmark_model import BenchmarkClassifier
from src.utils.config_manager import ConfigManager
from src.utils.gpu import set_best_gpu
from src.utils.logs import set_logger
from src.utils.prediction_results import PredictionResults
from src.utils.run_manager import RunManager

TMP_LOG_FILEPATH = ".console.log"
LOGGER_NAME = "src"


def run(
    run_config_file: Union[str, Path],
    debug: Optional[bool] = False,
    force_push: Optional[bool] = True,
    logging_level: Optional[int] = 20,
):
    """ Run training, prediction and results validation

    Parameters:
    -----------
    run_config_file: Union[str, Path]
        Path to the configuration file
    debug : Optional[bool]
        Flag to activate debug mode, allowing training with dirty
        repository, by default False
    force_push : Optional[bool]
        Flag to force push of git repository before training,
        by default True
    logging_level: Optional[int]
        Logging level
    """
    logger = logging.getLogger(LOGGER_NAME)
    set_logger(logger, logging_level, TMP_LOG_FILEPATH)

    config_manager = ConfigManager.load_config_file(run_config_file)
    logger.info("Config manager: PASSED")

    [train, test, valid], labels = create_subsets(**config_manager.data_pipeline.dict())
    set_best_gpu()
    run_manager = RunManager.from_parameters(config_manager, debug=debug)
    run_manager.log_mlflow_configfile(run_config_file)
    run_manager.log_mlflow_git_info(force_push=force_push)
    run_manager.log_system_info()

    # train model
    params = config_manager.hyperparameters.dict()

    model = BenchmarkClassifier(params)
    model.fit(train, valid)
    logger.info("Training: FINISHED")

    # evaluate
    pred_results = PredictionResults(model, test, labels)
    acc = pred_results.accuracy()
    loss = pred_results.log_loss()
    report = pred_results.classification_report()

    logger.info("loss: %s", round(loss, 4))
    logger.info("accuracy: %s", acc)
    logger.info("Classification report:\n %s", report)

    # save to MLFlow
    run_manager.log_classification_report(report)
    tracked_model_name = (
        f"{run_manager.mlflow_run_id_short}-{run_manager.model_filepath}"
    )
    model.save(run_manager.model_filepath)
    run_manager.log_mlflow_params(params)
    run_manager.log_mlflow_artifacts("catboost_info", "others")
    run_manager.set_mlflow_tag("mlflow.source.name", __file__)
    run_manager.log_mlflow_metrics(
        {"log_loss": round(loss, 4), "accuracy": round(acc, 4)}
    )
    run_manager.log_mlflow_logfile(TMP_LOG_FILEPATH)
    run_manager.stop_mlflow_run()


if __name__ == "__main__":
    fire.Fire(run)
