""" Benchmark model """
import logging
import os
from pathlib import Path
from typing import Dict, Union

from catboost import CatBoostClassifier, Pool

from src.models.base import BaseModel


logger = logging.getLogger(__name__)


class BenchmarkClassifier(BaseModel):
    """ Benchmark models based in CatBoost """

    def __init__(self, params: Dict):

        if "CUDA_VISIBLE_DEVICES" in os.environ:
            gpu_id = os.environ["CUDA_VISIBLE_DEVICES"]
            if gpu_id != "":
                params["task_type"] = "GPU"
                params["devices"] = gpu_id
                logger.info("Switching to GPU")
        super().__init__(params)

    def fit(self, train, valid=None, **kwargs):
        """
        Train model

        Parameters
        ----------
        train:
            Training data
        valid:
            Validation data

        Returns
        -------
        BenchmarkClassifier
        """
        self.model = CatBoostClassifier(**self.params)
        cat_features = None

        train_pool = Pool(train[0], train[1], cat_features=cat_features)
        validate_pool = Pool(valid[0], valid[1], cat_features=cat_features)
        self.model.fit(
            train_pool, eval_set=validate_pool, logging_level="Silent", plot=False
        )

        return self

    def predict(self, test):
        """
        Predict categories

        Parameters
        ----------
        test:
            Test data

        Returns
        -------
        numpy.array
        """
        y_pred = self.model.predict(test)
        return y_pred

    def predict_proba(self, test):
        """
        Predict porbabilities

        Parameters
        ----------
        test:
            Test data

        Returns
        -------
        numpy.array
        """
        y_proba = self.model.predict_proba(test)
        return y_proba

    def save(self, path_to_model: Union[str, Path]):
        """
        Save model

        Parameters
        ----------
        path_to_model: Union[str, Path]

        Returns
        -------
        None
        """
        path_to_model = Path(path_to_model).as_posix()
        self.model.save_model(path_to_model, format="cbm")

    def load(self, path_to_model: Union[str, Path]):
        """
        Save data

        Parameters
        ----------
        path_to_data: Union[str, Path]

        Returns
        -------
        """
        path_to_model = Path(path_to_model).as_posix()
        self.model = CatBoostClassifier()
        self.model.load_model(path_to_model)
        return self.model
