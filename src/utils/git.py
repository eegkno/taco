""" Module with git functionalities. """
import logging
import sys
from pathlib import Path
from typing import Optional, Sequence, Union

import git

logger = logging.getLogger(__name__)


class GitRepoNotFoundError(Exception):
    """ Raised when repo  is not found. """


def push():
    """ Push commits """
    repo = _init_repo()
    return repo.git.push()


def get_current_commit():
    """ Get current commit """
    repo = _init_repo()
    return repo.head.commit.hexsha


def is_repo_dirty(repo_path: Optional[Union[str, Path]] = None):
    """ Check if there are any non commited file in the repo """
    repo = _init_repo(repo_path)
    return repo.is_dirty()


def get_repo_url():
    """ Get remote origin url. """
    repo = _init_repo()
    remote_url = repo.remotes.origin.url
    return remote_url


def get_current_branch_name():
    """ Get current working branch name. """
    repo = _init_repo()
    branch = repo.active_branch
    return branch


def _get_main_file():
    filepath = Path(sys.argv[0])
    abs_filepath = filepath.absolute()

    return abs_filepath


def _init_repo(repo_path=None):
    if repo_path is None:
        repo_path = Path(__file__).parent

    try:
        repo = git.Repo(repo_path, search_parent_directories=True)
    except git.GitCommandError:
        raise GitRepoNotFoundError("Make sure the code is within a repo")
    return repo


def autocommit(
    file_paths: Sequence[Union[str, Path]] = None,
    mode: Optional[str] = "tracked",
    add_main: Optional[bool] = True,
    repo_path: Optional[Union[str, Path]] = None,
    message: Optional[str] = "AUTO COMMIT",
):
    """
    Autocommit files specified in file_paths plus additional files depending on
    the `mode` option. All commits are done on the active branch.
    If you are using jupyter notebooks you are better of passing the all files
    via the `file_paths` parameter, setting `add_main` to false and also
    passing the repo.

    Parameters:
    -----------
    file_paths: List
        Other files to track with path relatives to the root of the git repo.
    mode: str
        tracked/all/staged
    add_main: bool
        This is mainly for the conveniance of notebook users which may have trouble
        figuring out the filepath.
    repo_path:  List
        Path to repo or None
    message: str

    Returns:
    --------
        Last commit hash
    """
    supported_modes = ["all", "tracked", "stage"]
    mode = mode.lower()
    if mode not in supported_modes:
        raise Exception("Unsupported git autocommit mode.")

    try:
        repo = _init_repo(repo_path)
    except git.GitCommandError:
        return None

    # Track __main__
    if add_main:
        file_paths.append(_get_main_file())

    # Prepare arugments for commit
    commit_msg = f"-m {message}"
    if mode == "all":
        commit_params = ("--all", commit_msg)
    elif mode == "tracked":
        commit_params = (file_paths, commit_msg)
    elif mode == "staged":
        pass

    # Bare call to git add because otherwise filters are not apllied
    repo.git.add(file_paths)
    try:
        repo.git.commit(*commit_params)
    except git.GitCommandError:
        logger.info("Nothing new to commit")

    commit = repo.head.commit.hexsha
    return commit
