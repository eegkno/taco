""" Module containing custome strings."""

import datetime
import getpass
from typing import Optional


def date_time_string(
    with_username: bool = False,
    extra: Optional[str] = None,
    with_extension: Optional[str] = None,
) -> str:
    """
    Returns a formatted string as:
    {user_name}{date_format}-{time_format}{extra}{extension}

    Parameters
    ----------
    with_username : bool
        If True, username is detected, else it is empty.
    extra : str
        If not None, the string is included in the returned string.
    with_extension: str
        If not None, the string is included in the returned string.

    Returns
    -------
    str
        Human readable string.
    """
    current_time = datetime.datetime.now()
    date_format = (
        f"{current_time.year:04d}{current_time.month:02d}{current_time.day:02d}"
    )
    time_format = (
        f"{current_time.hour:02d}{current_time.minute:02d}{current_time.second:02d}"
    )

    user_name = f"{getpass.getuser()}-" if with_username else ""
    extra = f"-{extra}" if extra is not None else ""
    extension = with_extension if with_extension is not None else ""
    formatted_string = f"{user_name}{date_format}-{time_format}{extra}{extension}"

    return formatted_string
