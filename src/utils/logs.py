# -*- coding: utf-8 -*-
import logging
from pathlib import Path
from sys import stdout
from typing import Optional, Union

from coloredlogs import ColoredFormatter

PathType = Optional[Union[str, Path]]


def set_logger(
    logger: logging.Logger, level: int, logfile_path: PathType = None
) -> None:
    """Configure logger to use it as verbose mode in functions

    Parameters
    ----------
    logger : logging
        Logging which is not the root logger
    level : int
        Level of the logger.
    log_file : str or pathlib.Path
        Path to the logfile for a logging.FileHandler
    """

    logging.getLogger().setLevel(logging.DEBUG)
    logger.propagate = False
    logger.setLevel(logging.DEBUG)

    datefmt = "%y-%m-%d %H:%M:%S"
    message = "%(asctime)s - %(levelname)s - %(name)s::%(funcName)s(): %(message)s"
    message_formatter = logging.Formatter(fmt=message, datefmt=datefmt, style="%")

    colored_formatter = ColoredFormatter(message)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(colored_formatter)
    console_handler.setLevel(level)
    logger.addHandler(console_handler)

    if logfile_path is not None:
        Path(logfile_path).parent.mkdir(parents=True, exist_ok=True)
        file_handler = logging.FileHandler(logfile_path)
        file_handler.setFormatter(message_formatter)
        file_handler.setLevel(level)
        logger.addHandler(file_handler)

