import logging
import os
import warnings

import GPUtil


def set_best_gpu():
    """ Set first GPU avalibale based on ascending memory usage. """
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"

    device_id_list = []
    try:
        device_id_list = GPUtil.getFirstAvailable(order="memory")
    except RuntimeError:
        warning_msg = "No GPU available"
        warnings.warn(warning_msg)
        logging.warning(warning_msg)

    if not device_id_list:
        os.environ["CUDA_VISIBLE_DEVICES"] = ""
    else:
        os.environ["CUDA_VISIBLE_DEVICES"] = str(device_id_list[0])
