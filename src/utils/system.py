""" Module to get hardware and system information """
import logging
import os
import platform
import subprocess
from typing import List

import fire
import psutil

from src.utils.logs import set_logger

logger = logging.getLogger(__name__)


class CommandNotFoundError(Exception):
    """ Raised when command is not found. """


def get_system_info() -> str:
    """ Get system information

    Returns
    -------
    str
        Human readable message.

    """

    header = "\n===== System Information =====\n\n"
    uname = platform.uname()
    system = f"System: {uname.system}\n"
    node = f"Computer's name: {uname.node}\n"
    release = f"Release: {uname.release}\n"
    version = f"Version: {uname.version}\n"
    machine = f"Machine: {uname.machine}\n"
    processor = f"Processor: {uname.processor}\n"

    data_string = f"{header}{system}{node}{release}{version}{machine}{processor}"
    logger.debug(data_string)
    return data_string


def get_cpu_info() -> str:
    """ Get CPU info

    Returns
    -------
    str
        Human readable message.
    """
    header = "\n===== CPU Info =====\n\n"

    cores = f"Physical cores: {psutil.cpu_count(logical=False)}\n"
    total_cores = f"Total cores: {psutil.cpu_count(logical=True)}\n"

    # CPU frequencies
    cpufreq = psutil.cpu_freq()
    max_freq = f"Max Frequency: {cpufreq.max:.2f}Mhz\n"
    min_freq = f"Min Frequency: {cpufreq.min:.2f}Mhz\n"
    current_freq = f"Current Frequency: {cpufreq.current:.2f}Mhz\n"

    data_string = f"{header}{cores}{total_cores}{max_freq}{min_freq}{current_freq}"

    logger.debug(data_string)
    return data_string


def get_memory_info() -> str:
    """ Get memory info

    Returns
    -------
    str
        Human readable message.
    """
    header = "\n===== Memory Information =====\n\n"

    # memory details
    svmem = psutil.virtual_memory()
    total = f"Total: {_get_size(svmem.total)}\n"
    available = f"Available: {_get_size(svmem.available)}\n"
    used = f"Used: {_get_size(svmem.used)}\n"
    percentage = f"Percentage: {svmem.percent}%\n"

    # swap memory details
    subheader = "\nSWAP\n"
    swap = psutil.swap_memory()
    total_swap = f"Total: {_get_size(swap.total)}\n"
    free_swap = f"Free: {_get_size(swap.free)}\n"
    used_swap = f"Used: {_get_size(swap.used)}\n"
    percentage_swap = f"Percentage: {swap.percent}%\n"

    data_memory = f"{total}{available}{used}{percentage}"
    data_swap_memory = f"{total_swap}{free_swap}{used_swap}{percentage_swap}"

    data_string = f"{header}{data_memory}{subheader}{data_swap_memory}"
    logger.debug(data_string)
    return data_string


def _get_size(bytes_: float, suffix: str = "B") -> str:
    """
    Scale bytes to its proper format
    e.g:
        1253656 => '1.20MB'
        1253656678 => '1.17GB'
    """
    factor = 1024
    for unit in ["", "K", "M", "G", "T", "P"]:
        if bytes_ < factor:
            return f"{bytes_:.2f}{unit}{suffix}"
        bytes_ /= factor


def get_gpu_info(command: List[str]) -> str:
    """
    Get GPU info

    Parameters
    ----------
    command: List[str]
        nvidia-smi and flags

    Returns
    -------
    str
        Human readable message.
    """
    header = "\n===== GPU Information =====\n"

    if "CUDA_VISIBLE_DEVICES" not in os.environ:
        out = "\nNo GPU confgured, CPU will be used."
    else:
        gpu_id = os.environ["CUDA_VISIBLE_DEVICES"]
        if gpu_id == "":
            try:
                command = command + ["-i", gpu_id]
                logger.debug(" ".join(command))
                out = system_command(command)
            except CommandNotFoundError:
                out = f"\nCommand `{' '.join(command)}` not found, no info retrieved"
        else:
            out = "\nNo GPU available, CPU will be used."

    data_string = f"{header}{out}"
    logger.debug(data_string)
    return data_string


def system_command(command: List[str]) -> str:
    """
    Get the otuptu from a system command.

    Parameters
    ----------
    command: List[str]
        Command and flags

    Returns
    -------
    str
        Human readable message.

    Raises
    ------
    CommandNotFoundError

    """
    data_string = None
    try:
        sub_process = subprocess.Popen(
            command, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )

        out_string, _ = sub_process.communicate()
        out_list = out_string.decode("UTF-8").split("\\n")
        data_string = "\n" + "\n".join(out_list)
    except (FileNotFoundError, UnboundLocalError):
        raise CommandNotFoundError("System command not found error")

    return data_string


def get_container_info() -> str:
    """
    Get container info
    Returns
    -------
    str
        Human readable message.
    """
    header = "\n===== Container Information =====\n"

    try:
        command = ["info"]
        logger.debug(" ".join(command))
        out = system_command(command)
    except CommandNotFoundError:
        out = f"\nCommand `{' '.join(command)}` not found, no info retrieved"

    data_string = f"{header}{out}"
    logger.debug(data_string)
    return data_string


def run():
    """ Run all the functions in this module """
    get_system_info()
    get_cpu_info()
    get_memory_info()
    get_gpu_info(("ls", "-l"))
    get_container_info()

    os.environ["CUDA_VISIBLE_DEVICES"] = "1"
    get_gpu_info(["nvidia-smi", "-q"])

    os.environ["CUDA_VISIBLE_DEVICES"] = ""


if __name__ == "__main__":
    logger = logging.getLogger("src")
    set_logger(logger, 10)
    fire.Fire(run)
