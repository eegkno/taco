""" Module to validate yaml configuration file """
import getpass
from pathlib import Path
from typing import Optional, Union

from pydantic import BaseModel, validator

from src.utils.yaml_loader import ConfigLoader

PathType = Optional[Union[str, Path]]


class RunInfo(BaseModel):
    """ Class containing information about the current run (sprint, model name,
    and source of the training) """

    index_item: str
    model_name: str
    run_name: str
    source_type: str
    source_name: str
    model_file_extension = "pickle"
    mlflow_tracking_uri: str = "models"
    experiment_base_path: str = "data"
    repo_url: Optional[str] = None
    user: Optional[str] = None
    index_item_dir: Optional[str] = None
    experiment_path: PathType = None
    config_path: PathType = None
    model_path: PathType = None
    images_path: PathType = None
    results_path: PathType = None
    others_path: PathType = None
    checkpoints_path: PathType = None

    @validator("index_item")
    def index_item_must_not_contain_space(cls, value):
        """ Checks for no whitespace in index_item for MLflow """
        if " " in value:
            raise ValueError("must not contain whitespaces")
        return value.lower()

    @validator("model_name")
    def model_name_must_not_contain_space(cls, value):
        """ Checks for no whitespace in model name for MLflow """
        if " " in value:
            raise ValueError("must not contain whitespaces")
        return value.lower()

    @validator("model_file_extension")
    def extension_must_not_contain_dots(cls, value):
        """ Remove s in extension name for MLflow """
        return value.replace(".", "")

    @validator("run_name")
    def run_name_must_not_contain_space(cls, value):
        """ Checks for no whitespace in run name for MLflow """
        if " " in value:
            raise ValueError("must not contain whitespaces")
        return value.lower()

    @validator("source_type")
    def source_value(cls, source):
        """ Checks for allowed sources types """
        allowed_sources = ["NOTEBOOK", "SCRIPT"]
        if source.upper() not in allowed_sources:
            raise ValueError(f"must be within {' ,'.join(allowed_sources)}")
        return source.lower()

    @validator("user", pre=True, always=True)
    def set_user(cls, value):
        if value is None:
            value = getpass.getuser()
        return value

    @validator("index_item_dir", pre=True, always=True)
    def set_index_item_dir(cls, item_dir, values):
        if item_dir is None:
            try:
                item_dir = f"{values['index_item'].lower()}-data"
            except KeyError:
                raise ValueError("Missing attribute")
        return item_dir

    @validator("mlflow_tracking_uri", pre=True, always=True)
    def validate_mlflow_tracking_uri(cls, path):
        if not Path(path).exists():
            raise ValueError("`mlflow_tracking_uri` does not exist", path)
        return path

    @validator("experiment_base_path", pre=True, always=True)
    def validate_experiment_base_path(cls, path):
        if not Path(path).exists():
            raise ValueError("`experiment_base_path` does not exist", path)
        return path

    @validator("experiment_path", pre=True, always=True)
    def set_experiment_path(cls, path, values):
        if path is None:
            try:
                path = Path(
                    values["experiment_base_path"],
                    values["index_item_dir"],
                    values["user"],
                    values["model_name"],
                    values["run_name"],
                )
            except KeyError:
                raise ValueError("Missing attribute")
        else:
            path = Path(path)
        return path

    @staticmethod
    def _path_setter(path, values, dir_name):
        if path is None:
            try:
                path = Path(values["experiment_path"], dir_name)
            except KeyError:
                raise ValueError("Missing attribute `experiment_path`")
        else:
            path = Path(path)
        return path

    @validator("model_path", pre=True, always=True)
    def set_model_path(cls, path, values):
        return cls._path_setter(path, values, "model")

    @validator("images_path", pre=True, always=True)
    def set_images_path(cls, path, values):
        return cls._path_setter(path, values, "images")

    @validator("results_path", pre=True, always=True)
    def set_results_path(cls, path, values):
        return cls._path_setter(path, values, "results")

    @validator("others_path", pre=True, always=True)
    def set_others_path(cls, path, values):
        return cls._path_setter(path, values, "others")

    @validator("checkpoints_path", pre=True, always=True)
    def set_checkpoints_path(cls, path, values):
        return cls._path_setter(path, values, "checkpoints")

    @validator("config_path", pre=True, always=True)
    def set_config_path(cls, path, values):
        return cls._path_setter(path, values, "config")


class DataPipeline(BaseModel):
    """ Class containing the parameters to generate training, validation and
    test datasets.
    """

    random_state: int
    test_size: Optional[float]
    valid_size: Optional[float]


class Hyperparameters(BaseModel):
    """ Class containing all hyperparameters value """

    iterations: int
    learning_rate: float
    eval_metric: str
    random_seed: int
    logging_level: str
    use_best_model: bool
    od_type: str
    od_wait: int
    loss_function: str
    task_type: str
    devices: str
    save_snapshot: bool


class ConfigManager(BaseModel):
    """ Class to validate parameters from a config file """

    run_info: RunInfo
    data_pipeline: DataPipeline
    hyperparameters: Hyperparameters

    @classmethod
    def load_config_file(cls, config_file: Union[str, Path]):
        """ Create a ConfigManager from a config file. """
        config = ConfigLoader().load(config_file)
        return cls.parse_obj(config)
