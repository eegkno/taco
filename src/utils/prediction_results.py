""" Module to generate all the necessary results and reports from model """

import pandas as pd
from sklearn.metrics import accuracy_score, classification_report, log_loss


class PredictionResults:
    """ Predction Results class """

    def __init__(self, model, test, labels):
        self.labels = labels
        self.y_true = test[1]
        self.y_proba = model.predict_proba(test[0])
        self.y_pred = model.predict(test[0])

    def accuracy(self):
        """ Calculate accuracy. """
        return accuracy_score(self.y_true, self.y_pred)

    def log_loss(self):
        """ Calculate log loss. """
        return log_loss(self.y_true, self.y_proba)

    def classification_report(self):
        """ Generate classification report """
        report = classification_report(
            self.y_true,
            self.y_pred,
            output_dict=True,
            target_names=self.labels,
            labels=list(range(len(self.labels))),
        )

        return pd.DataFrame(report).transpose()
