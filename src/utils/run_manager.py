""" Module containing ResultsManager class which handles all training logging """

import logging
import os
from pathlib import Path
from typing import Dict, Union, Optional

import pandas as pd

import mlflow
from src.utils.config_manager import ConfigManager, RunInfo
from src.utils.format import date_time_string
from src.utils.git import (
    get_current_branch_name,
    get_current_commit,
    get_repo_url,
    is_repo_dirty,
    push,
)
from src.utils.system import (
    get_container_info,
    get_cpu_info,
    get_gpu_info,
    get_memory_info,
    get_system_info,
)

logger = logging.getLogger(__name__)
DEBUG_MESSAGE = "DEBUG_MODE"


class RunManager:
    """ Class to handle all training results, saving and logging everything with MLflow """

    def __init__(self, run_info: RunInfo, debug: Optional[bool] = False):
        """ Manage all training process. Results, save model, metrics and logs to MLflow.

        Parameters:
        -----------
        run_info : RunInfo
            All parameters regarding current run
        debug : Optional[bool]
            Flag to activate debug mode, allowing training with dirty
            repository, by default False
        """

        self.run_info = run_info
        self.debug = debug

        self._create_directories()

        self.formatted_string = date_time_string()
        self.exp_run_name = f"{self.run_info.model_name}-{self.run_info.run_name}"

        (
            self.mlflow_client,
            self.mlflow_run_id,
            self.artifact_uri,
        ) = self.start_mlflow_run()
        self.mlflow_run_id_short = (
            DEBUG_MESSAGE if self.debug else self.mlflow_run_id[:6]
        )

        log_filename = f"{self.mlflow_run_id_short}-{self.exp_run_name}-{self.formatted_string}.log"
        extension = self.run_info.model_file_extension
        model_filename = f"{self.mlflow_run_id_short}-{self.exp_run_name}-{self.formatted_string}.{extension}"
        self.log_filepath = Path(self.run_info.others_path, log_filename)
        self.model_filepath = Path(self.run_info.model_path, model_filename)

        self.set_mlflow_tag("mlflow.source.type", self.run_info.source_type)
        self.set_mlflow_tag("mlflow.source.name", self.run_info.source_name)
        self.set_mlflow_tag("mlflow_file_prefix", self.mlflow_run_id_short)
        self.set_mlflow_tag("mlflow_run_id", self.mlflow_run_id)
        self.set_mlflow_tag("mlflow_artifact_uri", self.artifact_uri)
        self.set_mlflow_tag(
            "mlflow_exp_path",
            f"{self.experiment.artifact_location}/{self.mlflow_run_id}",
        )
        self.set_mlflow_tag("model_name", self.run_info.model_name)
        self.set_mlflow_tag("run_name", self.run_info.run_name)

    def _create_directories(self):
        """ Create all directories for save paths """
        for parameter_name, path in self.run_info.dict().items():
            if parameter_name.endswith("_path"):
                Path(path).mkdir(parents=True, exist_ok=True)
        logger.info(
            "Local output directories ready in: %s", self.run_info.experiment_path
        )

    def save_model(self, model):
        """ Save model to default model path """
        model.save(self.model_filepath)
        logger.info("Saved model in %s", self.model_filepath)
        self.set_mlflow_tag("local_model_path", self.model_filepath)

    def start_mlflow_run(self):
        """ Create an experiment and a run """
        mlflow_client = mlflow.tracking.MlflowClient(
            tracking_uri=self.run_info.mlflow_tracking_uri
        )

        self.experiment = mlflow_client.get_experiment_by_name(self.run_info.index_item)
        if self.experiment is None:
            mlflow_client.create_experiment(self.run_info.index_item)
            self.experiment = mlflow_client.get_experiment_by_name(
                self.run_info.index_item
            )

        run = mlflow_client.create_run(
            self.experiment.experiment_id,
            tags={
                "mlflow.runName": f"{self.run_info.model_name}-{self.run_info.run_name}",
                "mlflow.user": self.run_info.user,
            },
        )
        run_id = run.info.run_id
        artifact_uri = run.info.artifact_uri
        logger.info(
            "Starting MLflow run with name '%s', id '%s', artifact_uri '%s'",
            self.run_info.run_name,
            run_id,
            artifact_uri,
        )

        return mlflow_client, run_id, artifact_uri

    def stop_mlflow_run(self):
        """ Stop the current run and log artifacts """
        logger.info(
            "Finishing MLflow run with name '%s' and id '%s'",
            self.run_info.run_name,
            self.mlflow_run_id,
        )
        for path in os.scandir(self.run_info.experiment_path):
            if path.is_dir():
                self.log_mlflow_artifacts(path.path, path.name)

        self.mlflow_client.set_terminated(self.mlflow_run_id)

    def log_mlflow_metrics(self, metrics_dict: Dict):
        """ Log all training metrics """

        for key, value in metrics_dict.items():
            self.mlflow_client.log_metric(
                run_id=self.mlflow_run_id, key=key, value=value
            )

    def log_mlflow_accuracy(self, accuracy: float):
        """ Log accuracy """
        self.mlflow_client.log_metric(self.mlflow_run_id, "overall_accuracy", accuracy)
        logger.info("Overall accuracy: %.4g", accuracy)

    def log_mlflow_param(self, key, value):
        """ Log a parameter with the given key value pair """
        self.mlflow_client.log_param(self.mlflow_run_id, key, value)

    def log_mlflow_params(self, metrics_dict: Dict):
        """ Log all training params """

        for key, value in metrics_dict.items():
            self.mlflow_client.log_param(
                run_id=self.mlflow_run_id, key=key, value=value
            )

    def log_mlflow_artifacts(
        self, local_path: str, artifact_path: Optional[str] = None,
    ):
        """ Log an artifact from the given local path to the artifact path if provided
        or to the default artifact path otherwise

        Parameters:
        -----------
        local_path: str
            Local path to store artifacts
        artifact_path: str
            Default artifact path
        """
        self.mlflow_client.log_artifacts(self.mlflow_run_id, local_path, artifact_path)

    def set_mlflow_tag(self, key, value):
        """Set a tag on the current run """
        self.mlflow_client.set_tag(self.mlflow_run_id, key, value)

    def log_mlflow_tags(self, tags_dict):
        """
        Logs all tags in MLflow run. All parameters must be named.
        """
        for key, value in tags_dict.items():
            self.set_mlflow_tag(key, value)

    def log_mlflow_configfile(self, run_config_file: Union[str, Path]):
        """ Log the configuration file """
        self.mlflow_client.log_artifact(self.mlflow_run_id, run_config_file, "config")

        # Rename file to add run_id
        config_name = Path(run_config_file).name
        old_name = f"{self.artifact_uri}/config/{config_name}"
        new_name = (
            f"{self.artifact_uri}/config/{self.mlflow_run_id_short}-{config_name}"
        )

        Path(old_name).rename(new_name)

    def log_mlflow_logfile(self, log_filepath: Union[str, Path]):
        """ Log the log file """
        Path(log_filepath).rename(self.log_filepath)

    def log_mlflow_git_info(self, force_push: Optional[bool] = True):
        """
        Check if the current repository is clean, push it, and log
        commit. If debug mode is activated, no check and no push is done.

        Parameters
        ----------
        force_push : bool, optional
            Flag to activate the push to git, by default True
        Raises
        ------
        RuntimeError
            In case the local repository is dirty (uncommitted changes)
        """
        if not self.debug:
            if is_repo_dirty():
                error_message = "Dirty repository, please commit changes"
                logger.error(error_message)
                raise RuntimeError(error_message)
            if force_push:
                # TODO: Check push's result. The result seems to be always an empty string
                push_result = push()
                if push_result == "":
                    logger.info("Nothing to push, remote directory is up-to-date")
                else:
                    logger.info("Pushed latest commits")
                    logger.debug("Push message : %s", push_result)
            commit = get_current_commit()
            logger.info("Commit and push experiment files")
        else:
            commit = DEBUG_MESSAGE
            logger.info("Debug mode, no git info")

        self.set_mlflow_tag("mlflow.source.git.commit", commit)
        self.set_mlflow_tag("repo_url", get_repo_url())
        self.set_mlflow_tag("branch", get_current_branch_name())

    def log_classification_report(self, report_dataframe: pd.DataFrame):
        """ Save evaluation report to default location """
        file_name = f"{self.mlflow_run_id_short}-eval_report-{self.exp_run_name}-{self.formatted_string}.csv"
        evaluation_report_filepath = Path(self.run_info.results_path, file_name,)
        report_dataframe.to_csv(
            evaluation_report_filepath, header=True, encoding="utf-8"
        )
        logger.info("Saved evaluation report in %s", evaluation_report_filepath)

    def log_system_info(self):
        """ Save system information to default location """

        system = get_system_info()
        cpu = get_cpu_info()
        memory = get_memory_info()
        gpu = get_gpu_info(["nvidia-smi"])
        container = get_container_info()

        file_name = f"{self.mlflow_run_id_short}-system_info-{self.exp_run_name}-{self.formatted_string}.txt"
        system_filepath = Path(self.run_info.others_path, file_name)
        data_stream = f"{system}{cpu}{memory}{container}{gpu}"

        Path(system_filepath).write_text(data_stream)
        logger.info("System info:\n %s", data_stream)
        logger.info("Saved system info in %s", system_filepath)

    @classmethod
    def from_parameters(
        cls, config_manager: ConfigManager, debug: Optional[bool] = False
    ):
        """
        Creates a RunManager instance from a ConfigManager instance.

        Parameters:
        -----------
        config_manager: ConfigManager
            Object for the current training run
        debug : Optional[bool]
            Flag to activate debug mode, allowing training with dirty
            repository, by default False

        Returns:
        --------
            RunManager
        """
        run_manager = cls(config_manager.run_info, debug)

        return run_manager
