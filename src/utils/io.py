""" Module with basic io functionalities """

import logging
import pickle
from pathlib import Path
from typing import Any, Union

logger = logging.getLogger(__name__)


def save_to_pickle(data, path_to_data: Union[str, Path]):
    """
    Save data

    Parameters:
    -----------
    path_to_data: Union[str, Path]

    Returns:
    --------
    None
    """
    with open(path_to_data, "wb") as file_:
        pickle.dump(data, file_, protocol=pickle.HIGHEST_PROTOCOL)
    logger.info("Data saved in %s", path_to_data)


def load_from_pickle(path_to_data: Union[str, Path]) -> Any:
    """
    Load data

    Parameters:
    -----------
    path_to_data: Union[str, Path]

    Returns:
    --------
    Any
    """
    with open(path_to_data, "rb") as file_:
        data = pickle.load(file_)
    logger.info("Load data from %s", path_to_data)
    return data
