import logging
from typing import Optional, Union

import fire
from sklearn import datasets
from sklearn.model_selection import train_test_split


logger = logging.getLogger(__name__)


def create_subsets(
    random_state: int,
    test_size: Optional[float] = 0.2,
    valid_size: Optional[float] = 0.2,
):
    iris = datasets.load_iris()
    X = iris.data
    y = iris.target
    labels = iris.target_names
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=test_size, random_state=random_state
    )
    X_train, X_valid, y_train, y_valid = train_test_split(
        X_train, y_train, test_size=valid_size, random_state=random_state
    )

    train = [X_train, y_train]
    test = [X_test, y_test]
    valid = [X_valid, y_valid]

    logger.info("train: %s", X_train.shape)
    logger.info("valid: %s", X_valid.shape)
    logger.info("test: %s", X_test.shape)
    return [train, test, valid], labels
